import datetime
import re 
import json
import sys

from bottle import Bottle, static_file
import pandas as pd

sys.path.append('../')
from utils.cors import enable_cors
from utils.utils import query_df, read_config

app = Bottle()
app.install(enable_cors)


def list_filter(config):
    ''' Matches a comma separated list of numbers. '''
    delimiter = ','
    regexp = r'\d+(%s\d)*' % re.escape(delimiter)

    def to_python(match):
        return map(int, match.split(delimiter))

    def to_url(numbers):
        return delimiter.join(map(str, numbers))

    return regexp, to_python, to_url

app.router.add_filter('list', list_filter)

barPlot_route = '/barPlot_route/<index:path:list>'
data = pd.read_csv("data/export_incidents.csv")
data["index"] = data.index
data["vitesse"] = data["vitesse"].str.split(" ", expand=True)[0]
data["vitesse"] = data["vitesse"].astype(int)
data = query_df(data, ligne=405000)

data_extract = data.loc[[100, 200, 500, 600, 800, 900, 1000], ["pk", "vitesse", "date", "index"]]

data_extract.to_json("data/data_extract.json", orient="records")

@app.route(barPlot_route)
def barPlot_graph(index):
    index = [int(element) for element in index.split(",")]
    
    dataRequest = data_extract[data_extract["index"].isin(index)]
    dataRequest["vitesse_bins"] = pd.cut(dataRequest["vitesse"], bins=3)
    
    barPlot_data = dataRequest.vitesse_bins.value_counts().rename("count").reset_index().to_json(orient="records")
    
    return barPlot_data

app.run(host="localhost", port="8000", debug=True)
