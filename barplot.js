var svg = d3.select("#bar-plot"),
    margin = {top: 20, right: 20, bottom: 30, left: 40},
    width = +svg.attr("width") - margin.left - margin.right,
    height = +svg.attr("height") - margin.top - margin.bottom;

var x = d3.scaleBand().rangeRound([0, width]).padding(0.1),
    y = d3.scaleLinear().rangeRound([height, 0]);

var g = svg.append("g")
	.attr("transform", "translate(" + margin.left + "," + margin.top + ")");


function brushended() {
    debugger;
    brushedData_index = brushedData_index_array(data);
    console.log(brushedData_index);
    request_data = "http://localhost:8000/barPlot_route/" + brushedData_index.join(",")
    
    d3.json(request_data, function(barPlot_data){
	
	
	
	if (error) throw error;

	x.domain(barPlot_data.map(function(d) { return d.index; }));
	y.domain([0, d3.max(barPlot_data, function(d) { return d.count; })]);

	g.append("g")
	    .attr("class", "axis axis--x")
	    .attr("transform", "translate(0," + height + ")")
	    .call(d3.axisBottom(x));

	g.append("g")
	    .attr("class", "axis axis--y")
	    .call(d3.axisLeft(y).ticks(10, "%"))
	    .append("text")
	    .attr("transform", "rotate(-90)")
	    .attr("y", 6)
	    .attr("dy", "0.71em")
	    .attr("text-anchor", "end")
	    .text("Frequency");

	g.selectAll(".bar")
	    .data(barPlot_data)
	    .enter().append("rect")
	    .attr("class", "bar")
	    .attr("x", function(d) { return x(d.index); })
	    .attr("y", function(d) { return y(d.count); })
	    .attr("width", x.bandwidth())
	    .attr("height", function(d) { return height - y(d.frequency); });
    });



    function brushedData_index_array(data){
	var brushed = [];
	data.forEach(function(e){ if (e.brushed) brushed.push(e.i);});
	return brushed;
    }    
}

